## Frameworks and tools used:
- Java 11
- Maven
- Selenium
- Cucumber
- Parallel Testing

# Instructions
1. Modify CHROME_VERSION in webstaurantstore_cucumber_page_factory.src.test.java.
com.webstaurantstore_cucumber_page_factory.infrastructure.driver.Setup with the 
current Chrome Version.

2. Run: mvn test -DbrowserName=chrome

## ToDo

1. WIP - Generate a better report
2. Add remote testing
2. Add videos recordings
3. It is not working for firefox, firefox_headless and safari(safari stopped working after latest MacOS update)
