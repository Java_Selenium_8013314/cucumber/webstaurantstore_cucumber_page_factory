package com.webstaurantstore_cucumber_page_factory.pages;

import org.openqa.selenium.WebDriver;

import com.webstaurantstore_cucumber_page_factory.infrastructure.driver.Setup;
import com.webstaurantstore_cucumber_page_factory.infrastructure.driver.Wait;

public class Page {

    protected WebDriver driver;
    protected Wait wait;

    public Page() {
        this.driver = Setup.driver;
        this.wait = new Wait(this.driver);
    }
    
    public String getTitle() {
	  return driver.getTitle();
    }
  
}
