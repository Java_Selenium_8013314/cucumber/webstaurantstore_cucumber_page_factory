package com.webstaurantstore_cucumber_page_factory.pages;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.NoSuchElementException;

import com.webstaurantstore_cucumber_page_factory.utils.Utilities;

public class CartPage extends Page {
	protected WebDriverWait wait;
	private final int DEFAULT_TIMEOUT = 10;
	
	@FindBy(xpath = "//input[@class='quantityInput input-mini ']")
	private List<WebElement> itemsQty;

	@FindBy(xpath = "//button[@class='emptyCartButton btn btn-mini btn-ui pull-right']")
	private WebElement emptyCartButton;

	@FindBy(css = "button.border-solid:nth-child(1)")
	private WebElement emptyCartButtonAlert;

	@FindBy(className = "header-1")
	private WebElement txtEmptyCart;
	
	@FindBy(xpath = "//h2[@id='empty-cart-title']")
	private WebElement emptyCartConfirmTitle;

	public CartPage() {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(DEFAULT_TIMEOUT));
	}
	
	public boolean getItemAdded(String itemDescription) {
		WebElement itemDescriptionLink = driver.findElement(By.xpath("//a[text()='" + itemDescription + "']"));

        return itemDescriptionLink.getText().equals(itemDescription);
    }

	public int getItemQty() {
		int qty = 0;
		if (itemsQty != null) {
			int itemsNo = itemsQty.size() - 1;
			WebElement itemQty = itemsQty.get(itemsNo);

			if (Utilities.isInteger(itemQty.getAttribute("value")))
				qty = Integer.parseInt(itemQty.getAttribute("value"));
		}
		return qty;
	}
	
	public String getEmptyCartButtonText() {
		return emptyCartButton.getText();
	}

	public void submitEmptyCartButton() {

		try {
			// Check if any elements were found
			if (emptyCartButton != null) {
				emptyCartButton.click();
			} else {
				System.out.println("No Empty Cart Button found!");
			}
		}
		catch (NoSuchElementException e) {
			System.err.println("No Empty Cart Button found!");
		}
	}
	
	public String getEmptyCartAlertTitle() {
		return emptyCartConfirmTitle.getText();
	}
	
	public void submitEmptyCartAlert() {
	
		try {
			// Check if any elements were found
			if (emptyCartButtonAlert != null) {
				emptyCartButtonAlert.click();
			} else {
			    System.out.println("No Empty Cart Button found!");
			}
		} 
		catch (NoSuchElementException e) {
			System.err.println("No Empty Cart Button found!");
		}
	}
	
	@SuppressWarnings("finally")
	public String emptyCartMessage() {
		String result = "";
	
		try {
			wait.until(ExpectedConditions.visibilityOf(txtEmptyCart));

			// Check if any elements were found
			if (txtEmptyCart != null) {
				result = txtEmptyCart.getText();
			} else {
			    System.out.println("'Your cart is empty.' paragraph not found!");
			}
		}
		catch (NoSuchElementException e) {
			System.err.println("'Your cart is empty.' paragraph not found!");
		}
		finally {
			return result;
		}
	}
}

