package com.webstaurantstore_cucumber_page_factory.pages;


import java.time.Duration;
import java.util.List;
import java.util.ArrayList;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.NoSuchElementException;

import com.webstaurantstore_cucumber_page_factory.utils.Utilities;

public class SearchPage extends Page {

	protected WebDriverWait wait;
	private final int DEFAULT_TIMEOUT = 10;
	private final String WRONG_TITLE = "Stainless Work Table - WebstaurantStore";

	@FindBy(css = "#paging > nav > ul > li.inline-block.leading-4.align-top.rounded-r-md > a")
	private WebElement nextPageButton;

	@FindBy(css = "#ProductBoxContainer" +
			":has(div.group.border-transparent.border-solid.border-6.m-0.max-w-full.relative.hover\\:outline-gray-200 > a > span)" +
			":has(div.add-to-cart > form > div > div > input.btn.btn-cart.btn-small)")
	private List<WebElement> products;

	@FindBy(css = "#ProductBoxContainer > div.group.border-transparent.border-solid.border-6.m-0.max-w-full.relative.hover\\:outline-gray-200 > a > span")
	private List<WebElement> productItems;

	@FindBy(name = "addToCartButton")
	private List<WebElement> addToCartButtons;
	
	@FindBy(xpath = "//h2[@class='notification__heading']")
	private WebElement viewCartAlert;
	
	@FindBy(xpath = "//div[@class='notification__description']")
	private WebElement itemAddedAlert;

	@FindBy(css = "a.btn.btn-small.btn-primary")
	private WebElement viewCartButton;
	
	public SearchPage() {
		PageFactory.initElements(driver, this);
		wait = new WebDriverWait(driver, Duration.ofSeconds(DEFAULT_TIMEOUT));
	}

	public boolean isLastPage() {
		try {
//			WebElement nextPageButton = driver.findElement(By.cssSelector("li.inline-block.leading-4.align-top.rounded-r-md > a")); // Assuming NEXT_PAGE_BUTTON is a String holding the CSS selector

			String href = nextPageButton.getAttribute("href");
			int pageNumber = Utilities.getPageNumber(href); // Assuming getPageNumber is defined elsewhere

			if (pageNumber > -1) {
				nextPageButton.click();
				return false;
			} else {
				return true;
			}
		} catch (NoSuchElementException e) {
			// Handle the case where the "next page" button is not found
			return true; // Assuming if the button is not found, it's the last page
		}
	}

	public ArrayList<String> searchResultByPage() throws NoSuchElementException {
		ArrayList<String> searchResults = new ArrayList<>();

		if (!productItems.isEmpty()) {
			for (WebElement element : productItems) {
				searchResults.add(element.getText());
			}
		}
		return searchResults;
	}

	public void goToLastItemsPage(){
		// Test navigating to the last page of search results
		int nextPage = 1;
		String href = "";

		while (nextPage > -1) {
			try {
				WebElement nextPageButton = driver.findElement(By.cssSelector("li.inline-block.leading-4.align-top.rounded-r-md > a")); // Assuming NEXT_PAGE_BUTTON is a String holding the CSS selector
				href = nextPageButton.getAttribute("href");
				nextPage = Utilities.getPageNumber(href);
				nextPageButton.click();
			} catch (NoSuchElementException e) {
				System.out.println("Reached last page.");
				break;
			}
		}
	}

	public String addLastItemToCart() {

		String itemAdded = "";

		try {

			if (products != null) {

				WebElement lastProduct = products.get(products.size() - 1);

				WebElement addToCartButton = lastProduct.findElement(By.name("addToCartButton"));
				WebElement item = lastProduct.findElement(By.cssSelector("div.group.border-transparent.border-solid.border-6.m-0.max-w-full.relative.hover\\:outline-gray-200 > a > span"));

				if (addToCartButton != null && item != null) {
					itemAdded = item.getText();
					addToCartButton.click();
					return itemAdded;
				}

			}
		}
		catch (NoSuchElementException e) {
			System.err.println("Can't add last item to cart");
		}
		return itemAdded;
	}

	public String addFirstItemToCart() {
		String itemAdded = "";

		try {

			if (addToCartButtons != null && productItems != null) {
				itemAdded = productItems.get(0).getText();
				addToCartButtons.get(0).click();
			}
		}
		catch (NoSuchElementException e) {
			System.err.println("Can't add last item to cart");
		}
		return itemAdded;
	}

	public String viewCartAlert() {
		try {

			wait.until(ExpectedConditions.visibilityOf(viewCartAlert));
			return viewCartAlert.getText();

		} catch (NoSuchElementException e) {
			System.out.println("View cart alert not found.");
			return "";
		}
	}

	public String getItemAddedAlert() {
		try {

			wait.until(ExpectedConditions.visibilityOf(itemAddedAlert));
			return itemAddedAlert.getText();

		} catch (NoSuchElementException e) {
			System.out.println("Item added message not found.");
			return "";
		}
	}
	
	public void submitViewCartButton() {
		wait.until(ExpectedConditions.visibilityOf(viewCartButton));
		viewCartButton.click();

		// Validate second click (optional): sometimes the item adding confirmation alert is displayed twice
		// If the title is equal to self.WRONG_TITLE then a second alert is displayed
		if (this.getTitle().startsWith(WRONG_TITLE))
			viewCartButton.click();
	}
}