package com.webstaurantstore_cucumber_page_factory.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

public class HomePage extends Page {
	
	private static final String HOME_PAGE_URL = "https://www.webstaurantstore.com/";

	@FindBy(id = "searchval")
	private WebElement searchBar;
	
	public HomePage() {
        PageFactory.initElements(driver, this);
    }
	
	public void openUrl() {
		driver.get(HOME_PAGE_URL);
	}
	
	public void searchProduct(String search) {
		searchBar.clear();
		searchBar.sendKeys(search);
		searchBar.submit();
	}

	public void openCart() {
		try {
	
			WebElement cartButton = driver.findElement(By.id("cartItemCountSpan"));
			
			if (cartButton != null) {
				cartButton.click();
			}
		}
		catch (NoSuchElementException e) {
			System.out.println("No Open Cart Button found");
		}
	}
}
