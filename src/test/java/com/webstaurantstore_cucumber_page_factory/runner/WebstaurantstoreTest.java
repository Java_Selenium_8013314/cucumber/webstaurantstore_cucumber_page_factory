package com.webstaurantstore_cucumber_page_factory.runner;

import io.cucumber.junit.CucumberOptions;
import io.cucumber.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = {
//				"src/test/features/com/webstaurantstore_cucumber_page_factory/Search_tables.feature"
//				"src/test/features/com/webstaurantstore_cucumber_page_factory/ShoppingCartSteps.feature"
				"src/test/features/com/webstaurantstore_cucumber_page_factory"
		}
		, plugin = {
				"pretty"
				, "json:target/reports_generator/json_reports/WebstaurantStore.json"
				, "json:target/cucumber_reports_generator/WebstaurantStore.json"
}
		, glue = {"com.webstaurantstore_cucumber_page_factory.infrastructure.driver"
			, "com.webstaurantstore_cucumber_page_factory.tests"
			, "com.webstaurantstore_cucumber_page_factory.reports"}
//		, parallel = true
		, tags = "@Regression"
)



public class WebstaurantstoreTest {
}
