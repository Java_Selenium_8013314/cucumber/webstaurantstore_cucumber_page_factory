package com.webstaurantstore_cucumber_page_factory.tests;

import com.webstaurantstore_cucumber_page_factory.pages.HomePage;
import com.webstaurantstore_cucumber_page_factory.pages.SearchPage;
import com.webstaurantstore_cucumber_page_factory.pages.CartPage;
import com.webstaurantstore_cucumber_page_factory.utils.Utilities;

import org.junit.Assert;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.util.ArrayList;

public class ShoppingCartSteps {
	
	final private HomePage homePage;
	final private SearchPage searchPage;
	final private CartPage cartPage;
	private String itemAdded;
	
	public ShoppingCartSteps() {
        this.homePage = new HomePage();
        this.searchPage = new SearchPage();
        this.cartPage = new CartPage();
    }

    @Given("A user open the \"Webstaurantstore\" website")
    public void a_user_open_the_webstaurantstore_website() {
        this.homePage.openUrl();
    }

    // Test Case: Add last item to cart

    @When("the user search and submit for {}")
    public void search_and_sumbit_for(String search) {
    	this.homePage.searchProduct(search);
    }

    @When("the user adds the last available item to the cart")
    public void user_adds_last_item_to_cart() {
        this.searchPage.goToLastItemsPage();
    	this.itemAdded = this.searchPage.addLastItemToCart();
    }

    @Then("the displayed search result title is {}")
    public void the_displayed_search_result_title_is(String searchTitle) {
        System.out.println("searchPage.getTitle(): " + searchPage.getTitle());
        System.out.println("searchTitle: " + searchTitle);

        Assert.assertEquals("Title is different than " + searchTitle, searchPage.getTitle(), searchTitle);

    }

    @Then("a item added confirmation alert is displayed with following legend {}")
    public void a_item_added_confirmation_alert_is_displayed(String viewAlert) {
        Assert.assertEquals("The " + viewAlert + " alert is not displayed", viewAlert, this.searchPage.viewCartAlert());
    }
    
    @And("the item description is displayed in the item added confirmation alert")
    public void the_item_description_is_diplayed_in_the_item_added_confirmation_alert() {
        Assert.assertEquals("The added item is " + this.searchPage.getItemAddedAlert()
                + " instead of " + this.itemAdded, this.itemAdded, this.searchPage.getItemAddedAlert());
    }

    @When("the user clicks on View Cart Button")
    public void the_user_clicks_on_view_cart_button() {
    	this.searchPage.submitViewCartButton(); 
    }

    @Then("the cart page is displayed with following title {}")
    public void cart_page_displayed(String cartPageTitle) {
        String displayedTitle = this.cartPage.getTitle();

        Assert.assertEquals("Displayed title is " + displayedTitle + " instead of " + cartPageTitle,
                cartPageTitle, displayedTitle);
    }

    @And("the item is displayed in the cart")
    public void item_displayed_in_the_cart() {
    	Assert.assertTrue(this.itemAdded + " is not found", this.cartPage.getItemAdded(this.itemAdded));
    }
    
    @And("item quantity is equal to {}")
    public void item_quantity_is_equal_to_1(String quantity) {
        int qty = 0;

        if (Utilities.isInteger(quantity))
            qty = Integer.parseInt(quantity);

        Assert.assertEquals("The added item quantity is " + this.cartPage.getItemQty() + " instead of " + qty,
                qty, this.cartPage.getItemQty());
    }
    
    @And("the following empty cart button is displayed: {}")
    public void empty_cart_button_is_displayed(String emptyCartButton) {
        Assert.assertEquals("The " + emptyCartButton + " button is not displayed",
                emptyCartButton, this.cartPage.getEmptyCartButtonText());
    }


    /* Test Case: Empty cart */
    @When("the user adds the first item to the cart")
    public void the_user_adds_the_first_item_to_the_cart() {
        this.itemAdded = this.searchPage.addFirstItemToCart();
    }

    @When("the user clicks on the \"Empty Cart\" button")
    public void user_clicks_empty_cart_button() {
        this.cartPage.submitEmptyCartButton();
        }

    @Then("the following confirmation dialog is displayed: {}")
    public void confirmation_dialog_is_displayed(String emptyCartAlertTitle) {
        Assert.assertEquals("The " + emptyCartAlertTitle + " alert is not displayed",
                emptyCartAlertTitle, this.cartPage.getEmptyCartAlertTitle());
    }

    @And("the user confirms emptying the cart")
    public void user_confirms_to_empty_cart() {
        this.cartPage.submitEmptyCartAlert();
    }

    @And("the cart is empty, and following message is displayed: {}")
    public void verify_if_cart_is_empty(String emptyCartMessage) {
        Assert.assertEquals("The empty cart message was not found", emptyCartMessage, this.cartPage.emptyCartMessage());

    }


    /* Verify if the cart is empty*/
    @When("the user clicks on \"cart\" button")
    public void user_clicks_cart_button() {
        this.homePage.openCart();
    }

}