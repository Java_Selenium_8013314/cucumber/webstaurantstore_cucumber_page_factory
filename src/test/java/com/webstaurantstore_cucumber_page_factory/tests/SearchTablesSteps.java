package com.webstaurantstore_cucumber_page_factory.tests;
import java.util.ArrayList;

import org.junit.Assert;

import com.webstaurantstore_cucumber_page_factory.pages.HomePage;
import com.webstaurantstore_cucumber_page_factory.pages.SearchPage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class SearchTablesSteps {
	
	private final HomePage homePage;
	private final SearchPage searchPage;
	
	public SearchTablesSteps() {
        this.homePage = new HomePage();
        this.searchPage = new SearchPage();
    }
	
	@Given("A user navigates to HomePage")
    public void a_user_navigates_to_HomePage() {
        this.homePage.openUrl();
    }

	@When("user search and submit for {}")
	public void user_search_and_submit(String search) {this.homePage.searchProduct(search);}
	
	@Then("all search result pages have the following title {} and all item descriptions contain the keyword {}")
	public void search_result_is_displayed(String searchTitle, String table) {
		System.out.println("searchPage.getTitle(): " + searchPage.getTitle());
		System.out.println("searchTitle: "+ searchTitle);

		Assert.assertEquals("Title is different than " + searchTitle, searchPage.getTitle(), searchTitle);
		int pageNo = 2;
		while (!searchPage.isLastPage()) {
			ArrayList<String> searchResults = searchPage.searchResultByPage();

//			Assert.assertEquals("Title is different than " + searchTitle, searchPage.getTitle(), searchTitle + " - Page " + pageNo);

			for (String item : searchResults) {
				Assert.assertTrue(item + " does not contains " + table + " word in the description", item.contains(table));
			}
			pageNo++;
		}
	}
		
	@And("all results contains the keyword {}")
	public void all_results_contains_the_keyword_table(String table) {

		while (!searchPage.isLastPage()) {
			ArrayList<String> searchResults = searchPage.searchResultByPage();

			for (String item : searchResults) {
				Assert.assertTrue(item + " does not contains " + table + " word in the description", item.contains(table));
			}
		}
	}

}
