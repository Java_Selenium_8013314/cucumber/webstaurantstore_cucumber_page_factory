package com.webstaurantstore_cucumber_page_factory.infrastructure.driver;

import io.cucumber.java.Before;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.safari.SafariOptions;

import io.github.bonigarcia.wdm.WebDriverManager;

import java.util.Map;

public class Setup {
    public static WebDriver driver;
    @Before
    public static void setWebDriver() {

        String browserName = System.getProperty("browserName");

        if (browserName == null) {
            browserName = "chrome";
        }

        ChromeOptions chromeOptions = new ChromeOptions();
        FirefoxOptions firefoxOptions = new FirefoxOptions();
        EdgeOptions edgeOptions = new EdgeOptions();
        SafariOptions safariOptions = new SafariOptions();

        if (browserName.startsWith("chrome")){

            chromeOptions.addArguments("--disable-notifications", "--ignore-certificate-errors", "--disable-extensions");
            chromeOptions.addArguments("disable-infobars");
            chromeOptions.addArguments("disable-dev-shm-usage");
            chromeOptions.addArguments("disable-features=VizDisplayCompositor");
            chromeOptions.addArguments("disable-features=IsolateOrigins,site-per-process");
            chromeOptions.addArguments("no-sandbox");
            chromeOptions.addArguments("disable-popup-blocking");
            chromeOptions.addArguments("enable-javascript");

            chromeOptions.setExperimentalOption("useAutomationExtension", false);
            chromeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});
            chromeOptions.setExperimentalOption("prefs", Map.of(
                    "profile.default_content_setting_values.notifications", 2,
                    "profile.default_content_setting_values.geolocation", 1,
                    "credentials_enable_service", false,
                    "profile.password_manager_enabled", false
            ));

            if(browserName.equals("chrome_headless")){
                chromeOptions.addArguments("headless=new");
                chromeOptions.addArguments("force-device-scale-factor=0.6");
            }
        } else
        if (browserName.startsWith("edge")){
            edgeOptions.addArguments("--disable-notifications", "--ignore-certificate-errors", "--disable-extensions");
            edgeOptions.addArguments("disable-infobars");
            edgeOptions.addArguments("disable-dev-shm-usage");
            edgeOptions.addArguments("disable-features=VizDisplayCompositor");
            edgeOptions.addArguments("disable-features=IsolateOrigins,site-per-process");

            edgeOptions.setExperimentalOption("useAutomationExtension", false);
            edgeOptions.setExperimentalOption("excludeSwitches", new String[]{"enable-automation"});

            if(browserName.equals("edge_headless")){
                edgeOptions.addArguments("headless=new");
                edgeOptions.addArguments("force-device-scale-factor=0.6");
            }
        } else
        if (browserName.startsWith("firefox")){
            firefoxOptions.addArguments("--disable-notifications","--ignore-certificate-errors");
            firefoxOptions.addArguments("--disable-infobars");
            firefoxOptions.addArguments("--enable-site-per-process=false");

            if(browserName.equals("firefox_headless")){
                firefoxOptions.addArguments("--headless");
                firefoxOptions.addArguments("force-device-scale-factor=0.6");
            }
        } else
        if (browserName.equals("safari"))
        {
            safariOptions.setAutomaticProfiling(true);
            safariOptions.setAutomaticInspection(true);

// Following Capabilities are deprecated, verify which are the new ones:

//    safariOptions.setCapability("enableRemoteAutomation", true);
//    safariOptions.setCapability("autoAcceptAlert", true); // Accept alerts automatically (adjust as needed)
//    safariOptions.setCapability("cleanSession", true); // Start a new session each time
//    safariOptions.setCapability("browserName", "safari");
//    safariOptions.setCapability("disable-features", "IsolateOrigins,site-per-process");
//      safariOptions.setCapability("safari.options.preferences",
//              "{\"WebKitPreferences\": {\"developerToolsEnabled\": false, \"javaScriptEnabled\": true}}");
//    safariOptions.setCapability("safari.options.preferences",
//            "{'WebKitPreferences': {'developerToolsEnabled': false, 'javaScriptEnabled': true}}");
//
//    safariOptions.setAutomaticInspection(false);
        }

        switch (browserName) {
            case "chrome", "chrome_headless":
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver(chromeOptions);
                break;
            case "edge", "edge_headless":
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver(edgeOptions);
                break;
            case "firefox", "firefox_headless":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver(firefoxOptions);
                break;
            case "safari":
                WebDriverManager.safaridriver().setup();
                driver = new SafariDriver(safariOptions);
                break;
            default:
                throw new IllegalArgumentException("Browser \"" + browserName + "\" isn't supported.");
        }
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();

    }

}
