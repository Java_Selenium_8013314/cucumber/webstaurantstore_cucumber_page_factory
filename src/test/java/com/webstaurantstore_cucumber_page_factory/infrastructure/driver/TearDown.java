package com.webstaurantstore_cucumber_page_factory.infrastructure.driver;

import io.cucumber.java.Scenario;
import io.cucumber.java.After;
import io.cucumber.java.AfterAll;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.webstaurantstore_cucumber_page_factory.reports.CucumberReportGenerator;

public class TearDown {

    private static WebDriver driver;
    org.apache.log4j.Logger log = Logger.getLogger(TearDown.class);

    public TearDown() {
        driver = Setup.driver;
    }

    @After
    public void quitDriver(Scenario scenario){
        if(scenario.isFailed()){
            log.info(scenario.getName() + " is Failed");
            saveScreenshotsForScenario(scenario);
        }
        if (driver != null) {
            driver.quit();
        }
    }

    @AfterAll
    public static void before_or_after_all() {
        if (driver != null) {
            driver.quit();
        }
//        CucumberReportGenerator.CucumberReportGenerator();
    }

    private void saveScreenshotsForScenario(final Scenario scenario) {

        final byte[] screenshot = ((TakesScreenshot) driver)
                .getScreenshotAs(OutputType.BYTES);

        scenario.attach(screenshot, "image/png", "embedded image");

    }

}

