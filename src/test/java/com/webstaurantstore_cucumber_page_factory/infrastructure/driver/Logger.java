package com.webstaurantstore_cucumber_page_factory.infrastructure.driver;

import org.apache.log4j.PropertyConfigurator;

public class Logger {

    private static boolean root=false;

    public static org.apache.log4j.Logger getLogger(Class cls){
        if(root){
            return org.apache.log4j.Logger.getLogger(cls);
        }
        PropertyConfigurator.configure("/Users/joel_arias/Documents/Coding/Testing/Front_End/Java/Cucumber" +
                "/Practice/webstaurantstore_cucumber_page_factory/src/main/resources/log4j.properties");

        root = true;
        return org.apache.log4j.Logger.getLogger(cls);
    }

}