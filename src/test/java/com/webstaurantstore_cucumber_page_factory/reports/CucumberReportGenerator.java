package com.webstaurantstore_cucumber_page_factory.reports;

// Remove from POM if I don't achieve it: dependency and plugin
import net.masterthought.cucumber.Configuration;
import net.masterthought.cucumber.ReportBuilder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CucumberReportGenerator {
    public static void CucumberReportGenerator() {

        File reportOutputDirectory = new File("target/cucumber_reports_generator");
        List<String> jsonFiles = new ArrayList<>();
//        Review
        jsonFiles.add("target/cucumber_reports_generator/WebstaurantStore.json");
//        jsonFiles.add("target/reports_generator/json_reports/WebstaurantStore.json");



        String projectName = "WebstaurantStore";
        Configuration configuration = new Configuration(reportOutputDirectory, projectName);

        ReportBuilder reportBuilder = new ReportBuilder(jsonFiles, configuration);
        reportBuilder.generateReports();
    }
}