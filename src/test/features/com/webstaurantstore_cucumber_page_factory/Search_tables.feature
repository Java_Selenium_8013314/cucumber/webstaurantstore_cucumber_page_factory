#Author: joel.arias@gmail.com

@Regression @CheckTables
Feature: Search tables
  To check if all results of searching "stainless work table" have the keyword "table".


  @SearchTables 
  Scenario Outline: Check page "Table" keyword
    Given A user navigates to HomePage
    When user search and submit for <search>
    Then all search result pages have the following title <search_page_title> and all item descriptions contain the keyword <expected_token>
    Examples:
      | search               | expected_token | search_page_title                       |
      | stainless work table | Table          | Stainless Work Table - WebstaurantStore |
