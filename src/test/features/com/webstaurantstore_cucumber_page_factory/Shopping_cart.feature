#Author: joel.arias@gmail.com

@Regression @ShoppingCart
Feature: Shopping_cart
  Perform cart operations

	Background:
  	Given A user navigates to HomePage

  @AddLastItemToCart
  Scenario Outline: Add last item to cart
    When the user search and submit for <search>
    Then the displayed search result title is <search_page_title>
    When the user adds the last available item to the cart
    Then a item added confirmation alert is displayed with following legend <view_alert>
    And the item description is displayed in the item added confirmation alert
    When the user clicks on View Cart Button
    Then the cart page is displayed with following title <cart_page_title>
    And the item is displayed in the cart
    And item quantity is equal to <item_qty>
    And the following empty cart button is displayed: <empty_cart_button>
    Examples:
      | search               | search_page_title                       | view_alert                | cart_page_title       |item_qty| empty_cart_button |
      | stainless work table | Stainless Work Table - WebstaurantStore | 1 item added to your cart | WebstaurantStore Cart |1       | Empty Cart        |

  @EmptyCart
  Scenario Outline: Empty a shopping cart with at least one item
    When the user search and submit for <search>
    Then the displayed search result title is <search_page_title>
    When the user adds the first item to the cart
    Then a item added confirmation alert is displayed with following legend <view_alert>
    And the item description is displayed in the item added confirmation alert
    When the user clicks on View Cart Button
    Then the cart page is displayed with following title <cart_page_title>
    And the item is displayed in the cart
    And the following empty cart button is displayed: <empty_cart_button>
  	When the user clicks on the "Empty Cart" button
  	Then the following confirmation dialog is displayed: <empty_cart_alert_title>
    And the user confirms emptying the cart
  	Then the cart is empty, and following message is displayed: <empty_cart_message>
    Examples:
      | search               | search_page_title                       | view_alert                | cart_page_title       | empty_cart_button | empty_cart_alert_title | empty_cart_message  |
      | stainless work table | Stainless Work Table - WebstaurantStore | 1 item added to your cart | WebstaurantStore Cart | Empty Cart        | Empty Cart             | Your cart is empty. |


  Scenario Outline: Verify if the cart is empty
    When the user clicks on "cart" button
    Then the cart page is displayed with following title <cart_page_title>
    And the cart is empty, and following message is displayed: <empty_cart_message>
    Examples:
      | cart_page_title       | empty_cart_message  |
      | WebstaurantStore Cart | Your cart is empty. |
